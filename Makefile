lint:
	cargo fmt --all --verbose -- --check
build:
	cargo build

test:
	cargo test -- --nocapture
fix:
	cargo fix
