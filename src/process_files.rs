#[allow(unused_imports)]
use async_std::prelude::*;

#[allow(unused_imports)]
use std::fs::{read_dir, read_to_string, remove_dir_all, remove_file, rename, DirEntry, File};

#[allow(unused_imports)]
use std::path::Path;

use glob::Pattern;
use walkdir::WalkDir;

// Pattern that indicates if a file should be used as a replacement.
pub const FILE_MARKER: &str = ".replacement";

// Name of ignore file.
pub const IGNORE_FILE_NAME: &str = ".templateignore";

pub trait FileProcessor {
    fn process_files(&self, files: &[&str]) -> Result<(), std::io::Error>;
}

pub async fn process_files(directory: &str) -> Result<(), std::io::Error> {
    let ignored_patterns = get_ignored_patterns(directory)?;
    let (remove, replace): (Vec<String>, Vec<String>) =
        get_file_paths(directory)?.into_iter().partition(|path| {
            let relative_path = &path.replace(&format!("{}/", directory), "");
            match_path(relative_path, &ignored_patterns)
        });
    replace_files(replace);
    remove_files(remove);
    remove_ignored_directories(directory, &ignored_patterns)
}

fn remove_ignored_directories(
    directory: &str,
    patterns: &Vec<Pattern>,
) -> Result<(), std::io::Error> {
    WalkDir::new(directory)
        .into_iter()
        .filter_map(|e| e.ok())
        .filter(|e| e.path().is_dir())
        .filter_map(|e| e.path().as_os_str().to_os_string().into_string().ok())
        .filter(|dir_path| {
            let relative_path = &dir_path.replace(&format!("{}/", directory), "");
            match_path(relative_path, patterns)
        })
        .try_for_each(|dir_path| remove_dir_all(dir_path))
}

fn match_path(path: &str, patterns: &Vec<Pattern>) -> bool {
    patterns.iter().any(|pattern| pattern.matches(path))
}

fn replace_files(files: Vec<String>) -> Result<(), std::io::Error> {
    files
        .iter()
        .try_for_each(|file_path| rename(file_path, file_path.replace(FILE_MARKER, "")))
}

fn remove_files(files: Vec<String>) -> Result<(), std::io::Error> {
    files
        .iter()
        .try_for_each(|file_path| remove_file(file_path))
}

pub fn get_file_paths(directory: &str) -> Result<Vec<String>, std::io::Error> {
    Ok(WalkDir::new(directory)
        .into_iter()
        .filter_map(|e| e.ok())
        .filter(|e| e.path().is_file())
        .filter_map(|e| e.path().as_os_str().to_os_string().into_string().ok())
        .collect())
}

fn get_ignored_patterns(path: &str) -> Result<Vec<Pattern>, std::io::Error> {
    let user_ignored_patterns: Vec<Pattern> =
        read_to_string(format!("{}/{}", path, &IGNORE_FILE_NAME))
            .unwrap_or_else(|_| String::from(""))
            .split("\n")
            .chain(std::iter::once(IGNORE_FILE_NAME))
            .filter_map(|p| Pattern::new(p).ok())
            .collect();
    Ok(user_ignored_patterns)
}

#[cfg(test)]
mod tests {
    use super::*;
    use tempfile::tempdir;
    use test_utils::create_files;

    #[allow(unused_imports)]
    use async_std::prelude::*;

    #[allow(unused_imports)]
    use std::fs::{create_dir, create_dir_all, read_dir, read_to_string, remove_dir_all, File};

    #[allow(unused_imports)]
    use std::io::Write;

    #[allow(unused_must_use)]
    #[test]
    fn can_get_all_files() -> Result<(), std::io::Error> {
        let test_dir = tempdir()?
            .path()
            .as_os_str()
            .to_os_string()
            .into_string()
            .unwrap_or_else(|_| String::from(""));

        let some_dir = format!("{}/some-dir", &test_dir);

        // Files are created at multi directory levels.
        create_files(&[
            (format!("{}/myfile", &test_dir), String::from("")),
            (format!("{}/myfile", &some_dir), String::from("")),
        ])?;

        let all_files = get_file_paths(&test_dir)?;

        // Verify that we only got two files, as only two were created.
        assert_eq!(all_files.len() as i32, 2);

        // Verify that the correct files have been found.
        let verify_existance = |correct_path: &str| {
            assert!(
                all_files.iter().any(|path| path == correct_path),
                "Missing file: {}",
                correct_path
            );
        };

        verify_existance(&format!("{}/myfile", &test_dir));
        verify_existance(&format!("{}/myfile", &some_dir));

        Ok(())
    }

    #[async_std::test]
    async fn can_replace_dot_file() -> Result<(), std::io::Error> {
        let test_dir = tempdir()?
            .path()
            .as_os_str()
            .to_os_string()
            .into_string()
            .unwrap_or_else(|_| String::from(""));

        create_files(&vec![
            (format!("{}/.myfile", &test_dir), String::from("")),
            (
                format!("{}/.myfile{}", &test_dir, FILE_MARKER),
                String::from("keep"),
            ),
        ])?;

        process_files(&test_dir).await?;

        let validate_path = |path: &str| -> Result<(), std::io::Error> {
            assert_eq!(path, &format!("{}/.myfile", &test_dir));
            assert_eq!(read_to_string(path)?, "keep");
            Ok(())
        };

        read_dir(&test_dir)?
            .filter_map(|result| result.ok())
            .for_each(|entry| {
                match entry.path().to_str() {
                    Some(v) => validate_path(v),
                    _ => Ok(()),
                };
            });

        Ok(())
    }

    #[async_std::test]
    async fn can_replace_dot_file_with_type() -> Result<(), std::io::Error> {
        let test_dir = tempdir()?
            .path()
            .as_os_str()
            .to_os_string()
            .into_string()
            .unwrap_or_else(|_| String::from(""));

        create_files(&vec![
            (format!("{}/.myfile.type", &test_dir), String::from("")),
            (
                format!("{}/.myfile{}.type", &test_dir, FILE_MARKER),
                String::from("keep"),
            ),
        ])?;

        process_files(&test_dir).await?;

        // Verify that there is only one file.
        let file_paths = get_file_paths(&test_dir)?;
        assert_eq!(file_paths.len() as i32, 1);

        // Verify that it is the correct file.
        assert_eq!(&file_paths[0], &format!("{}/.myfile.type", &test_dir));
        assert_eq!(read_to_string(&file_paths[0])?, "keep");

        Ok(())
    }

    #[async_std::test]
    async fn can_process_files_in_a_multi_level_directory() -> Result<(), std::io::Error> {
        let test_dir = tempdir()?
            .path()
            .as_os_str()
            .to_os_string()
            .into_string()
            .unwrap_or_else(|_| String::from(""));

        let some_dir = format!("{}/some-dir", &test_dir);

        create_files(&vec![
            (format!("{}/myfile", &test_dir), String::from("")),
            (
                format!("{}/myfile{}", &test_dir, FILE_MARKER),
                String::from("keep"),
            ),
            (format!("{}/myfile", &some_dir), String::from("")),
            (
                format!("{}/myfile{}", &some_dir, FILE_MARKER),
                String::from("keep"),
            ),
        ])?;

        process_files(&test_dir).await?;

        // Find all paths
        let paths: Vec<String> = get_file_paths(&test_dir)?;

        // There should be 2 paths, one for each file.
        assert_eq!(paths.len() as i32, 2);

        let validate_file = |path: &String| -> Result<(), std::io::Error> {
            // Verify that file exists
            assert!(
                &paths.iter().any(|path| path == path),
                format!("Missing file: {}", &path)
            );

            // Verify that the file has been replaced
            assert_eq!(
                read_to_string(path)?,
                "keep",
                "{} does not have correct contents \"keep\"",
                &path
            );
            Ok(())
        };

        // Validate that both files have been replaced
        validate_file(&format!("{}/myfile", &test_dir))?;
        validate_file(&format!("{}/myfile", &some_dir))?;

        Ok(())
    }

    #[async_std::test]
    async fn can_ignore_files() -> Result<(), std::io::Error> {
        let file_name_1 = String::from("tobeignored1.txt");
        let file_name_2 = String::from("tobeignored2.txt");
        let test_dir = tempdir()?
            .path()
            .as_os_str()
            .to_os_string()
            .into_string()
            .unwrap_or_else(|_| String::from(""));

        create_files(&vec![
            (format!("{}/{}", &test_dir, &file_name_1), String::from("")),
            (format!("{}/{}", &test_dir, &file_name_2), String::from("")),
            (
                format!("{}/{}", &test_dir, &IGNORE_FILE_NAME),
                format!("{}\n{}", &file_name_1, &file_name_2),
            ),
        ])?;

        assert_eq!(get_file_paths(&test_dir)?.len() as i32, 3);

        process_files(&test_dir).await?;

        let remaining_files = get_file_paths(&test_dir)?;

        assert_eq!(
            get_file_paths(&test_dir)?.len() as i32,
            0,
            "Expected all files to be ignored, file paths left: {:?}",
            remaining_files
        );

        Ok(())
    }

    #[async_std::test]
    async fn can_ignore_directories() -> Result<(), std::io::Error> {
        let test_dir = tempdir()?
            .path()
            .as_os_str()
            .to_os_string()
            .into_string()
            .unwrap_or_else(|_| String::from(""));
        let dir_name = "some-dir";

        create_files(&vec![
            (
                format!("{}/{}/{}", &test_dir, &dir_name, "some-file"),
                format!("{}", "some text"),
            ),
            (
                format!("{}/{}", &test_dir, &IGNORE_FILE_NAME),
                format!("{}", &dir_name),
            ),
        ])?;

        process_files(&test_dir).await?;

        assert_eq!(read_dir(test_dir)?.count() as i32, 0);
        Ok(())
    }

    #[test]
    fn can_get_ignored_patterns() -> Result<(), std::io::Error> {
        let test_dir = tempdir()?
            .path()
            .as_os_str()
            .to_os_string()
            .into_string()
            .unwrap_or_else(|_| String::from(""));

        let user_ignored_globs: &[String] = &[String::from("mypath/*"), String::from("somefile")];
        let mandatory_ignored_globs: &[String] = &[String::from(IGNORE_FILE_NAME)];

        create_files(&vec![(
            format!("{}/{}", &test_dir, &IGNORE_FILE_NAME),
            user_ignored_globs.join("\n"),
        )])?;

        let ignored_patterns = get_ignored_patterns(&test_dir)?;

        assert_eq!(
            user_ignored_globs
                .iter()
                .chain(mandatory_ignored_globs)
                .filter_map(|glob| Pattern::new(glob).ok())
                .collect::<Vec<Pattern>>(),
            ignored_patterns.as_slice()
        );

        Ok(())
    }
}
