mod fetch_template;
#[allow(unused_must_use)]
mod process_files;
mod process_template_pairs;

use async_std::task;
use fetch_template::{create_git_fetcher, create_git_tag_fetcher};
use process_files::{get_file_paths, process_files, FileProcessor};
use process_template_pairs::TemplatePairProcessor;
use structopt::StructOpt;

#[derive(StructOpt)]
struct Cli {
    // Target directory for project.
    #[structopt()]
    target: String,

    #[structopt()]
    source: String,

    #[structopt(short, long)]
    tag: Option<String>,

    #[structopt(long)]
    template_pairs: Option<Vec<String>>,
}

fn main() -> Result<(), std::io::Error> {
    let args = Cli::from_args();
    let fetcher = match &args.tag {
        Some(tag) => create_git_tag_fetcher(&args.source, &args.target, &tag),
        None => create_git_fetcher(&args.source, &args.target),
    };
    let template_pair_processor = match &args.template_pairs {
        Some(template_pairs) => {
            let template_pair_strs = template_pairs
                .iter()
                .map(|val| val.as_str())
                .collect::<Vec<&str>>();
            Some(TemplatePairProcessor::new(template_pair_strs.as_slice()))
        }
        None => None,
    };

    task::block_on(async {
        fetcher()?;
        process_files(&args.target).await?;
        if let Some(template_pair_processor) = template_pair_processor {
            let file_paths = get_file_paths(&args.target)?;
            let file_path_strings = file_paths
                .iter()
                .map(|path| path.as_str())
                .collect::<Vec<&str>>();
            template_pair_processor.process_files(file_path_strings.as_slice())
        } else {
            Ok(())
        }
    })
}
