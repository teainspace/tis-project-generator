#[allow(unused_imports)]
use async_std::prelude::*;

use git2::Repository;
use std::fs::remove_dir_all;

pub type Fetcher = Box<dyn Fn() -> Result<(), std::io::Error>>;

pub fn create_git_fetcher(source: &str, target: &str) -> Fetcher {
    let src = source.to_string();
    let tgt = target.to_string();
    let lol = move || fetch_template_from_git(&src, &tgt);
    Box::new(lol)
}

pub fn create_git_tag_fetcher(source: &str, target: &str, tag: &str) -> Fetcher {
    let src = source.to_string();
    let tgt = target.to_string();
    let tag = tag.to_string();
    let lol = move || fetch_template_from_git_tag(&src, &tgt, &tag);
    Box::new(lol)
}

/// Fetch template fetchs template from a git directory.
/// The .git directory is removed.
pub fn fetch_template_from_git(source: &str, target: &str) -> Result<(), std::io::Error> {
    match Repository::clone(source, target) {
        Err(e) => panic!("failed to clone: {}", e),
        _ => (),
    };

    remove_dir_all(format!("{}/.git", target))
}

pub fn fetch_template_from_git_tag(
    source: &str,
    target: &str,
    tag: &str,
) -> Result<(), std::io::Error> {
    let repository = match Repository::clone(source, target) {
        Err(e) => panic!("failed to clone: {}", e),
        Ok(repository) => repository,
    };

    let tag_ref = match repository.resolve_reference_from_short_name(tag) {
        Err(e) => panic!("failed to find tag oid: {}", e),
        Ok(tag_oid) => tag_oid,
    };

    let tag = match tag_ref.peel_to_tree() {
        Err(e) => panic!("failed to find tag {}", e),
        Ok(tag) => tag,
    };

    match repository.checkout_tree(&tag.as_object(), None) {
        Err(e) => panic!("Failed to checkout tag: {}", e),
        _ => (),
    };

    remove_dir_all(format!("{}/.git", target))
}

#[cfg(test)]
mod tests {
    use super::*;
    use git2::{Commit, IndexAddOption, ObjectType, Signature};
    use std::fs::{read_dir, read_to_string};
    use tempfile::tempdir;
    use test_utils::{add_and_commit_all_files, create_files, tag_latest_commit};

    #[allow(unused_imports)]
    use async_std::prelude::*;

    #[async_std::test]
    async fn can_fetch_template_from_git_repo_path() -> Result<(), Box<dyn std::error::Error>> {
        let source_dir = tempdir()?
            .path()
            .as_os_str()
            .to_os_string()
            .into_string()
            .unwrap_or_else(|_| String::from(""));

        let target_dir = tempdir()?
            .path()
            .as_os_str()
            .to_os_string()
            .into_string()
            .unwrap_or_else(|_| String::from(""));

        let repo = match Repository::init(&source_dir) {
            Ok(repo) => (repo),
            _ => panic!("Failed to init repo in {}", &source_dir),
        };

        create_files(&[(format!("{}/myfile", &source_dir), String::from("mycontent"))])?;

        add_and_commit_all_files(&repo, "test", &Signature::now("test", "test@test")?, &[])?;

        let fetcher = create_git_fetcher(&source_dir, &target_dir);

        fetcher();
        // Verify that the git directory is removed, as it is not included in the template.
        let dir_entries = read_dir(&target_dir)?;
        dir_entries
            .filter_map(|entry| entry.ok())
            .for_each(|entry| {
                let path = entry
                    .path()
                    .into_os_string()
                    .into_string()
                    .unwrap_or_else(|_| String::from(""));
                assert!(!path.contains(".git"));
            });

        // Verify that there are files in the template.
        assert!(read_dir(&target_dir)?.count() == 1);

        Ok(())
    }

    #[async_std::test]
    async fn can_fetch_template_from_git_repo_path_with_tag(
    ) -> Result<(), Box<dyn std::error::Error>> {
        let source_dir = tempdir()?
            .path()
            .as_os_str()
            .to_os_string()
            .into_string()
            .unwrap_or_else(|_| String::from(""));

        let target_dir = tempdir()?
            .path()
            .as_os_str()
            .to_os_string()
            .into_string()
            .unwrap_or_else(|_| String::from(""));

        let repo = match Repository::init(&source_dir) {
            Ok(repo) => (repo),
            _ => panic!("Failed to init repo in {}", &source_dir),
        };

        create_files(&[(format!("{}/myfile", &source_dir), String::from("mycontent"))])?;

        let first_commit_oid =
            add_and_commit_all_files(&repo, "test", &Signature::now("test", "test@test")?, &[])?;

        let my_tag = "my_tag";
        tag_latest_commit(my_tag, &repo)?;
        create_files(&[(
            format!("{}/myfile2", &source_dir),
            String::from("mycontent2"),
        )])?;
        let repo2 = match Repository::discover(&source_dir) {
            Ok(repo) => (repo),
            _ => panic!("Failed to init repo in {}", &source_dir),
        };
        add_and_commit_all_files(
            &repo2,
            "test",
            &Signature::now("test", "test@test")?,
            &[first_commit_oid],
        )?;
        let fetcher = create_git_tag_fetcher(&source_dir, &target_dir, my_tag);
        fetcher();
        // Verify that the git directory is removed, as it is not included in the template.
        let dir_entries = read_dir(&target_dir)?;
        dir_entries
            .filter_map(|entry| entry.ok())
            .for_each(|entry| {
                let path = entry
                    .path()
                    .into_os_string()
                    .into_string()
                    .unwrap_or_else(|_| String::from(""));
                assert!(!path.contains(".git"));
            });
        // Verify that there are files in the template.

        assert!(
            read_dir(&target_dir)?
                .filter_map(|entry| entry.ok())
                .filter(|entry| {
                    let path = entry
                        .path()
                        .into_os_string()
                        .into_string()
                        .unwrap_or_else(|_| String::from(""));
                    !path.contains(".git")
                })
                .count()
                == 1
        );

        read_dir(&target_dir)?
            .filter_map(|entry| entry.ok())
            .try_for_each(|entry| -> Result<(), std::io::Error> {
                let path = entry
                    .path()
                    .into_os_string()
                    .into_string()
                    .unwrap_or_else(|_| String::from(""));
                Ok(assert_eq!(read_to_string(&path)?, "mycontent"))
            });
        Ok(())
    }
}
