use crate::process_files::FileProcessor;
use std::collections::HashMap;
use std::fs::{read_to_string, write};
use std::path::Path;

pub struct TemplatePairProcessor {
    template_pairs: HashMap<String, String>,
}

impl TemplatePairProcessor {
    pub fn new(templating_args: &[&str]) -> TemplatePairProcessor {
        let mut templating_args_mutable = templating_args.to_vec();
        let mut template_pairs = HashMap::new();

        while templating_args_mutable.len() > 0 {
            let value = templating_args_mutable.pop().unwrap().to_string();
            let key = templating_args_mutable.pop().unwrap().to_string();
            template_pairs.insert(key, value);
        }

        TemplatePairProcessor { template_pairs }
    }

    fn replace_template_pairs(&self, path: &str) -> Result<(), std::io::Error> {
        let updated_contents = self
            .template_pairs
            .iter()
            .fold(read_to_string(Path::new(path))?, |acc, (key, value)| {
                acc.replace(key, value)
            });

        write(path, updated_contents)
    }
}

impl FileProcessor for TemplatePairProcessor {
    fn process_files(&self, paths: &[&str]) -> Result<(), std::io::Error> {
        paths
            .into_iter()
            .try_for_each(|path| self.replace_template_pairs(path))
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use std::fs::read_to_string;
    use std::path::Path;
    use tempfile::tempdir;
    use test_utils::create_files;

    #[test]
    fn can_apply_template_pairs() -> Result<(), std::io::Error> {
        let template_pairs: HashMap<String, String> = [
            (String::from("{{REPLACE_ME}}"), String::from("value1")),
            (String::from("{{TO_BE_REPLACED}}"), String::from("value2")),
        ]
        .iter()
        .cloned()
        .collect();

        let template_pair_args: Vec<&str> =
            template_pairs.iter().fold(vec![], |mut acc, (key, value)| {
                acc.push(key);
                acc.push(value);
                acc
            });
        let keys: Vec<String> = template_pairs.keys().cloned().collect();

        let test_dir = tempdir()?
            .path()
            .as_os_str()
            .to_os_string()
            .into_string()
            .unwrap_or_else(|_| String::from(""));

        let some_dir = format!("{}/some-dir", &test_dir);
        let files = [
            (format!("{}/myfile", &test_dir), keys[0].clone()),
            (format!("{}/myfile", &some_dir), keys[1].clone()),
        ];
        // Files are created at multi directory levels.
        create_files(&files)?;

        let template_pair_processor = TemplatePairProcessor::new(template_pair_args.as_slice());
        let actual_files: Vec<&str> = files.iter().map(|(path, _)| path.as_str()).collect();
        template_pair_processor.process_files(actual_files.as_slice())?;

        assert_eq!(
            &read_to_string(Path::new(&files[0].0))?,
            template_pairs.get(&files[0].1).unwrap_or(&String::from(""))
        );
        assert_eq!(
            &read_to_string(Path::new(&files[1].0))?,
            template_pairs.get(&files[1].1).unwrap_or(&String::from(""))
        );

        Ok(())
    }
}
