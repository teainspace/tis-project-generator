use git2::{Commit, IndexAddOption, ObjectType, Repository, Signature};
use std::fs::{create_dir_all, File};
use std::io::Write;
use std::path::Path;

pub fn create_files(files: &[(String, String)]) -> Result<(), std::io::Error> {
    files
        .iter()
        .map(|file| &file.0)
        .map(Path::new)
        .filter_map(|path| path.ancestors().nth(1))
        .filter_map(|path| path.to_str())
        .try_for_each(create_dir_all)?;

    files
        .iter()
        .try_for_each(|file| File::create(&file.0)?.write_all(file.1.as_bytes()))
}

pub fn add_and_commit_all_files(
    repo: &Repository,
    message: &str,
    signature: &Signature,
    parents: &[git2::Oid],
) -> Result<git2::Oid, git2::Error> {
    let mut index = repo.index()?;
    index.add_all(["*"].iter(), IndexAddOption::DEFAULT, None);
    let oid = index.write_tree()?;

    let parent_commits: Vec<git2::Commit> = parents
        .iter()
        .filter_map(|oid| repo.find_commit(*oid).ok())
        .collect();

    let tree = repo.find_tree(oid)?;
    repo.commit(
        Some("HEAD"),
        &signature,
        &signature,
        message,
        &tree,
        parent_commits
            .iter()
            .collect::<Vec<&git2::Commit>>()
            .as_slice(),
    )
}

pub fn tag_latest_commit(tag_name: &str, repo: &Repository) -> Result<git2::Oid, git2::Error> {
    let object = repo.revparse_single("HEAD")?;
    repo.tag_lightweight(tag_name, &object, true)
}
