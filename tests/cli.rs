use assert_cmd::prelude::*;
use git2::{IndexAddOption, Repository, Signature};
use std::collections::HashMap;
use std::error::Error;
use std::fs::read_to_string;
use std::path::Path;
use std::process::Command;
use tempfile::tempdir;
use test_utils::{add_and_commit_all_files, create_files, tag_latest_commit};

const FILE_MARKER: &str = ".replacement";

#[test]
fn can_apply_template_from_git_tag() -> Result<(), Box<dyn Error>> {
    let source_dir = tempdir()?
        .path()
        .as_os_str()
        .to_os_string()
        .into_string()
        .unwrap_or_else(|_| String::from(""));

    let target_dir = tempdir()?
        .path()
        .as_os_str()
        .to_os_string()
        .into_string()
        .unwrap_or_else(|_| String::from(""));

    let template_pairs: HashMap<String, String> = [
        (String::from("{{REPLACE_ME}}"), String::from("value1")),
        (String::from("{{TO_BE_REPLACED}}"), String::from("value2")),
    ]
    .iter()
    .cloned()
    .collect();

    let template_pairs_as_list: Vec<&str> =
        template_pairs.iter().fold(vec![], |mut acc, (key, value)| {
            acc.push(key);
            acc.push(value);
            acc
        });

    let repo = match Repository::init(&source_dir) {
        Ok(repo) => (repo),
        _ => panic!("Failed to init repo in {}", &source_dir),
    };

    let file_paths = [
        (format!("{}/myfile.type", &source_dir), String::from("")),
        (
            format!("{}/myfile{}.type", &source_dir, FILE_MARKER),
            String::from("keep"),
        ),
        (
            format!("{}/ignoreme.type", &source_dir),
            String::from("keep"),
        ),
        (
            format!("{}/mydir/afile.type", &source_dir),
            String::from(""),
        ),
        (
            format!("{}/mydir/afile{}.type", &source_dir, FILE_MARKER),
            String::from("keep"),
        ),
    ];

    let file_paths_for_second_commit = [(
        format!("{}/myfile2.type", &source_dir),
        String::from("keep"),
    )];

    create_files(&file_paths)?;

    let first_commit_oid =
        add_and_commit_all_files(&repo, "test", &Signature::now("test", "test@test")?, &[])?;

    let my_tag = "my_tag";
    tag_latest_commit(my_tag, &repo);

    create_files(&file_paths_for_second_commit);

    add_and_commit_all_files(
        &repo,
        "test",
        &Signature::now("test", "test@test")?,
        &[first_commit_oid],
    );

    let output = Command::cargo_bin("tis_project_generator")?
        .arg(&target_dir)
        .arg(&source_dir)
        .arg("--tag")
        .arg(&my_tag)
        .output()?;

    assert!(&output.status.success(), "Output: {:?}", &output);

    let remaining_files: Vec<(String, String)> = file_paths
        .into_iter()
        .map(|file| {
            (
                String::from(&file.0.replace(&source_dir, &target_dir)),
                String::from(&file.1),
            )
        })
        .filter(|file| Path::new(&file.0).is_file())
        .collect();

    let remaining_files_for_second_commit: Vec<(String, String)> = file_paths_for_second_commit
        .into_iter()
        .map(|file| {
            (
                String::from(&file.0.replace(&source_dir, &target_dir)),
                String::from(&file.1),
            )
        })
        .filter(|file| Path::new(&file.0).is_file())
        .collect();

    assert_eq!(
        remaining_files.len() as i32,
        3,
        "Remaining files: {:?}",
        &remaining_files
    );

    remaining_files
        .iter()
        .try_for_each(|file| -> Result<(), std::io::Error> {
            Ok(assert_eq!(read_to_string(Path::new(&file.0))?, "keep"))
        })?;

    remaining_files_for_second_commit.iter().try_for_each(
        |file| -> Result<(), std::io::Error> {
            Ok(assert!(
                !Path::new(&file.0).is_file(),
                "File {:?} was found",
                &file
            ))
        },
    )?;

    Ok(())
}

#[test]
fn can_apply_template_from_git_repository() -> Result<(), Box<dyn Error>> {
    let source_dir = tempdir()?
        .path()
        .as_os_str()
        .to_os_string()
        .into_string()
        .unwrap_or_else(|_| String::from(""));

    let target_dir = tempdir()?
        .path()
        .as_os_str()
        .to_os_string()
        .into_string()
        .unwrap_or_else(|_| String::from(""));

    let repo = match Repository::init(&source_dir) {
        Ok(repo) => (repo),
        _ => panic!("Failed to init repo in {}", &source_dir),
    };

    let file_paths = [
        (format!("{}/myfile.type", &source_dir), String::from("")),
        (
            format!("{}/myfile{}.type", &source_dir, FILE_MARKER),
            String::from("keep"),
        ),
        (
            format!("{}/ignoreme.type", &source_dir),
            String::from("keep"),
        ),
        (
            format!("{}/mydir/afile.type", &source_dir),
            String::from(""),
        ),
        (
            format!("{}/mydir/afile{}.type", &source_dir, FILE_MARKER),
            String::from("keep"),
        ),
    ];

    create_files(&file_paths)?;

    add_and_commit_all_files(&repo, "test", &Signature::now("test", "test@test")?, &[])?;

    let output = Command::cargo_bin("tis_project_generator")?
        .arg(&target_dir)
        .arg(&source_dir)
        .output()?;

    assert!(&output.status.success(), "Output: {:?}", &output);

    let remaining_files: Vec<(String, String)> = file_paths
        .into_iter()
        .map(|file| {
            (
                String::from(&file.0.replace(&source_dir, &target_dir)),
                String::from(&file.1),
            )
        })
        .filter(|file| Path::new(&file.0).is_file())
        .collect();

    assert_eq!(
        remaining_files.len() as i32,
        3,
        "Remaining files: {:?}",
        &remaining_files
    );

    remaining_files
        .iter()
        .try_for_each(|file| -> Result<(), std::io::Error> {
            Ok(assert_eq!(read_to_string(Path::new(&file.0))?, "keep"))
        })?;

    Ok(())
}

#[test]
fn can_apply_multiple_template_pairs() -> Result<(), Box<dyn Error>> {
    let source_dir = tempdir()?
        .path()
        .as_os_str()
        .to_os_string()
        .into_string()
        .unwrap_or_else(|_| String::from(""));

    let target_dir = tempdir()?
        .path()
        .as_os_str()
        .to_os_string()
        .into_string()
        .unwrap_or_else(|_| String::from(""));

    let template_pairs: HashMap<String, String> = [
        (String::from("{{REPLACE_ME}}"), String::from("value1")),
        (String::from("{{TO_BE_REPLACED}}"), String::from("value2")),
    ]
    .iter()
    .cloned()
    .collect();

    let repo = match Repository::init(&source_dir) {
        Ok(repo) => (repo),
        _ => panic!("Failed to init repo in {}", &source_dir),
    };

    let template_keys = vec![
        "{{REPLACE_ME}}",
        "{{REPLACE_ME_YOU_WILL}}",
        "{{TO_BE_REPLACED}}",
    ];
    let template_values = vec!["value1", "value2", "value3"];

    let file_paths = [
        (format!("{}/myfile.type", &source_dir), String::from("")),
        (
            format!("{}/myfile{}.type", &source_dir, FILE_MARKER),
            format!("keep {}", &template_keys[0]),
        ),
        (
            format!("{}/ignoreme.type", &source_dir),
            format!("keep {}", &template_keys[1]),
        ),
        (
            format!("{}/mydir/afile.type", &source_dir),
            String::from(""),
        ),
        (
            format!("{}/mydir/afile{}.type", &source_dir, FILE_MARKER),
            format!("keep {}", &template_keys[2]),
        ),
    ];

    create_files(&file_paths);

    let first_commit_oid =
        add_and_commit_all_files(&repo, "test", &Signature::now("test", "test@test")?, &[])?;

    let template_key_value_pairs =
        template_keys
            .into_iter()
            .enumerate()
            .fold(vec![], |mut acc, (index, key)| {
                acc.push(key);
                acc.push(&template_values[index]);
                acc
            });

    let output = Command::cargo_bin("tis_project_generator")?
        .arg(&target_dir)
        .arg(&source_dir)
        .arg("--template-pairs")
        .args(&template_key_value_pairs)
        .output()?;

    assert!(&output.status.success(), "Output: {:?}", &output);

    // Next step check file contents.
    let remaining_files: Vec<(String, String)> = file_paths
        .into_iter()
        .map(|file| {
            (
                String::from(&file.0.replace(&source_dir, &target_dir)),
                String::from(&file.1),
            )
        })
        .filter(|file| Path::new(&file.0).is_file())
        .collect();

    assert_eq!(
        read_to_string(Path::new(&remaining_files[0].0))?,
        format!("keep {}", template_values[0])
    );

    assert_eq!(
        read_to_string(Path::new(&remaining_files[1].0))?,
        format!("keep {}", template_values[1])
    );

    assert_eq!(
        read_to_string(Path::new(&remaining_files[2].0))?,
        format!("keep {}", template_values[2])
    );

    Ok(())
}
